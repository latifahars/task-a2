// console.log('Hello World');
// console.error('This is an error');
// console.warn('This is a warning');

//--------------------------------------------
// var, let, const

// let age = 30;
// age = 31;
// console.log();

// const age = 30; //tidak bisa
// age = 31;
// console.log();

// let score;
// score = 10;
// console.log(score);

// const score = 10;
// console.log(score);

//------------------------------------------------
//STRING, NUMBERS, BOOLEAN, NULL, UNDEFINED, SYMBOL

// const name = 'John'; //string
// const age = 30; //numbers
// const rating = 4.5; //numbers
// const isCool = true; //boolean
// const x = null; //object
// const y = undefined;
// let z; //undefined

// console.log(typeof x);

//------------------------------------------------
//STRING

// const name = 'John';
// const age = 30;

// console.log('My name is ' + name + ' and I am ' + age); 
// const hello = `My name is ${name} and I am ${age}`;
// console.log(hello);

//------------------------------------------------
// COUPLE STRING

// const s = 'Hello World!';
// console.log(s.length);
// console.log(s.toUpperCase()); //menampilkan s dg huruf kapital
// console.log(s.toLowerCase()); //menampilkan s dg huruf kecil
// console.log(s.substring(0,5)); //menampilkan 0-5 huruf
// console.log(s.substring(0,5).toUpperCase());
// console.log(s.split(''));

// const t = 'technology, computers, it, code';
// console.log(t.split(','));

//-----------------------------------------------------
//ARRAYS

// const numbers = new Array(1,2,3,4,5);
// console.log(numbers);

// const fruits = ['apples', 'oranges', 'pears'];
// fruits[3] = 'grape';
// fruits.push('mangos'); //input belakang
// fruits.unshift('strawberries'); //input depan
// fruits.pop(); //menampilkan tanpa urutan terakhirnya
// console.log(Array.isArray(fruits)); // cek type
// console.log(fruits.indexOf('oranges')); //cek index ke berapa
// console.log(fruits);

//------------------------------------------------------
// OBJECT LITERALS

// const person = {
//   firstName: 'John',
//   lastName: 'Doe',
//   age: 30,
//   hobbies: ['music', 'movies', 'sports'],
//   address: {
//     street: '50 Main st',
//     city: 'Boston',
//     state: 'MA'
//   }
// }

// console.log(person.firstName, person.lastName);
// console.log(person.address.city);

// const { firstName, lastName, address: {city} } = person;
// console.log(city);

// person.email = 'john@gmail.com';
// console.log(person);

//---------------------------------------------------------
// ARRAY OF OBJECT

// const todos = [
//   {
//     id: 1,
//     text: 'Take out trash',
//     isComplete: true
//   },
//   {
//     id: 2,
//     text: 'Meeting with boss',
//     isComplete: true
//   },
//   {
//     id: 3,
//     text: 'Dentist appt',
//     isComplete: false
//   }
// ];

// console.log(todos);
// console.log(todos[1].text);

// const todoJSON = JSON.stringify(todos);
// console.log(todoJSON);

// // For
// for(let i = 0; i <= 10; i++){
//   console.log(`For Loop Number: ${i}`);
// }

// // While
// let i = 0
// while(i <= 10) {
//   console.log(`While Loop Number: ${i}`);
//   i++;
// }

// // For Loop
// for(let i = 0; i < todos.length; i++){
//   console.log(todos[i].text);
// }

// for(let todo of todos) {
//   console.log(todo.text);
// }

//--------------------------------------------
// FOREACH, MAP, FILTER
// todos.forEach(function(todo){
// 	console.log(todo.text);
// })

// const todoText = todos.map(function(todo){
// 	return todo.text;
// });
// console.log(todoText);

// const todoCompleted = todos.filter(function(todo){
// 	return todo.isComplete === true;
// }).map(function(todo){
// 	return todo.text;
// })
// console.log(todoCompleted);\

//---------------------------------------------
// CONDITIONAL
// const x = 6;
// const y = 11;

// if(x > 5 && y > 10) {
//   console.log('x is more then 5 or y is more then 10');
// }

// const x = 9;
// const color = 'green';
// switch(color) {
// 	case 'red':
// 		console.log('color is red');
// 		break;
// 	case 'blue':
// 		console.log('color is blue');
// 		break;
// 	default:
// 		console.log('color is NOT red or blue');
// 		break;
// }

//----------------------------------------------------
// const addNums = (num1 = 1, num2 = 1) => {
// 	return num1 + num2;
// }
// console.log(addNums(5, 5));

// const addNums = num1 => num1 + 5;
// console.log(addNums(5));

//-------------------------------------------------------
// OBJECT ORIENTED PROGRAMMING

//constructor function
// function Person(firstName, lastName, dob) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   this.dob = new Date(dob);
//   // this.getBirthYear = function(){
//   // 	return this.dob.getFullYear();
//   // }
//   // this.getFullName = function(){
//   // 	return `${this.firstName} ${this.lastName}`;
//   // }
// }

// Person.prototype.getBirthYear = function () {
//   return this.dob.getFullYear();
// }
// Person.prototype.getFullName = function() {
//   return `${this.firstName} ${this.lastName}`
// }

// //CLASS
// class Person {
//   constructor(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = new Date(dob);
//   }
//   // Get Birth Year
//   getBirthYear() {
//     return this.dob.getFullYear();
//   }

//   // Get Full Name
//   getFullName() {
//     return `${this.firstName} ${this.lastName}`
//   }
// }

// // Instantiate object
// const person1 = new Person('John', 'Doe', '4-3-1980');
// const person2 = new Person('Mary', 'Smith', '3-6-1970');

// console.log(person2.getFullName());
// console.log(person1);
// console.log(person2.dob.getFullYear());

//--------------------------------------------------------
//--------------------------------------------------------
//DOM

// console.log(window);
// window.alert(1);

//single element
// console.log(document.getElementById('my-form'));
// console.log(document.querySelector('h1'));
//multiple element
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByClassName('item'));
// console.log(document.getElementsByTagName('li'));

// const items = document.querySelectorAll('.item');
// items.forEach((item) => console.log(item));

// const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// const btn = document.querySelector('.btn');
// btn.style.background = 'red';

//  const btn = document.querySelector('.btn');
// // btn.addEventListener('click', (e) => {
// // 	e.preventDefault();
// // 	console.log(e.target.className);
// // });

// btn.addEventListener('click', (e) => {
// 	e.preventDefault();
// 	document.getElementById('my-form').style.background = '#ccc';
// 	document.querySelector('body').classList.add('bg-dark');
// 	document.querySelector('.items').lastElementChild.innerHTML = '<h1>Changed</h1>';
// });

//---------------------------------------------------------

// USER FORM SCRIPT
// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}
